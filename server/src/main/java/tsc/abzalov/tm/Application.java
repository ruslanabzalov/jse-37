package tsc.abzalov.tm;

import org.jetbrains.annotations.NotNull;
import tsc.abzalov.tm.bootstrap.CommandBootstrap;

public final class Application {

    public static void main(@NotNull final String... args) {
        new CommandBootstrap();
    }

}
