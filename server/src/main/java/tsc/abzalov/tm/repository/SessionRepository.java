package tsc.abzalov.tm.repository;

import lombok.SneakyThrows;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.repository.ISessionRepository;
import tsc.abzalov.tm.model.Session;

import java.sql.Connection;
import java.sql.Types;

public final class SessionRepository implements ISessionRepository {

    @NotNull
    private final Connection connection;

    public SessionRepository(@NotNull final Connection connection) {
        this.connection = connection;
    }

    @Override
    @SneakyThrows
    public void addSession(@NotNull final Session session) {
        @NotNull val query = "INSERT INTO session (id, timestamp, signature, user_id) VALUES (?, ?, ?, ?);";
        @NotNull val preparedStatement = this.connection.prepareStatement(query);
        preparedStatement.setLong(1, session.getId());
        preparedStatement.setObject(2, session.getTimestamp(), Types.BIGINT);
        preparedStatement.setString(3, session.getSignature());
        preparedStatement.setObject(4, session.getUserId(), Types.BIGINT);
        preparedStatement.executeUpdate();
    }

    @Override
    @SneakyThrows
    public void removeSession(@NotNull final Long id) {
        @NotNull val query = "DELETE FROM session WHERE id = ?;";
        @NotNull val preparedStatement = this.connection.prepareStatement(query);
        preparedStatement.setLong(1, id);
        preparedStatement.executeUpdate();
    }

    @Override
    @Nullable
    @SneakyThrows
    public Session findSession(@NotNull final Long id) {
        @NotNull val query = "SELECT id, timestamp, signature, user_id FROM session WHERE id = ?;";
        @NotNull val preparedStatement = this.connection.prepareStatement(query);
        preparedStatement.setLong(1, id);
        @NotNull val result = preparedStatement.executeQuery();

        if (!result.next()) return null;
        @NotNull val foundedSession = new Session();
        foundedSession.setId(result.getLong(1));
        foundedSession.setTimestamp(result.getLong(2));
        foundedSession.setSignature(result.getString(3));
        foundedSession.setUserId(result.getLong(4));
        return foundedSession;
    }

}
