package tsc.abzalov.tm.repository;

import lombok.SneakyThrows;
import lombok.val;
import lombok.var;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.repository.IUserRepository;
import tsc.abzalov.tm.enumeration.Role;
import tsc.abzalov.tm.model.User;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @NotNull
    private final Connection connection;

    public UserRepository(@NotNull final Connection connection) {
        this.connection = connection;
    }

    @Override
    @SneakyThrows
    public long size() {
        @NotNull val query = "SELECT COUNT(*) FROM user;";
        @NotNull val statement = this.connection.createStatement();
        @NotNull val result = statement.executeQuery(query);

        var count = 0L;
        if (result.next()) count = result.getLong(1);
        return count;
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    @SneakyThrows
    public void create(@NotNull final User user) {
        @NotNull val query = "INSERT INTO user (id, login, password, role, firstname, lastname, email) VALUES (?, ?, ?, ?, ?, ?, ?);";
        @NotNull val prepareStatement = this.connection.prepareStatement(query);

        prepareStatement.setLong(1, user.getId());
        prepareStatement.setString(2, user.getLogin());
        prepareStatement.setString(3, user.getHashedPassword());
        prepareStatement.setString(4, user.getRole().toString());
        prepareStatement.setString(5, user.getFirstName());
        prepareStatement.setString(6, user.getLastName());
        prepareStatement.setString(7, user.getEmail());

        prepareStatement.executeUpdate();
    }

    @Override
    public void addAll(@NotNull final List<User> users) {
        users.forEach(this::create);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<User> findAll() {
        @NotNull val query = "SELECT id, login, password, role, firstname, lastname, email, locked_flg FROM user;";
        @NotNull val statement = this.connection.createStatement();
        @NotNull val result = statement.executeQuery(query);

        @NotNull val users = new ArrayList<User>();
        while (result.next()) users.add(createUser(result));
        return users;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findById(@NotNull final Long id) {
        @NotNull val query = "SELECT id, login, password, role, firstname, lastname, email, locked_flg FROM user WHERE id = ?;";
        @NotNull val prepareStatement = this.connection.prepareStatement(query);

        prepareStatement.setLong(1, id);
        @NotNull val result = prepareStatement.executeQuery();

        if (!result.next()) return null;
        return createUser(result);
    }

    @Override
    @SneakyThrows
    @SuppressWarnings("SqlWithoutWhere")
    public void clear() {
        @NotNull val query = "DELETE FROM user;";
        @NotNull val statement = this.connection.createStatement();
        statement.executeUpdate(query);
    }

    @Override
    @SneakyThrows
    public void removeById(@NotNull final Long id) {
        @NotNull val query = "DELETE FROM user WHERE id = ?;";
        @NotNull val prepareStatement = this.connection.prepareStatement(query);

        prepareStatement.setLong(1, id);
        prepareStatement.executeUpdate();
    }

    @Override
    @Nullable
    @SneakyThrows
    public User findByLogin(@NotNull final String login) {
        @NotNull val query = "SELECT id, login, password, role, firstname, lastname, email, locked_flg FROM user WHERE login = ?;";
        @NotNull val prepareStatement = this.connection.prepareStatement(query);
        prepareStatement.setString(1, login);
        @NotNull val result = prepareStatement.executeQuery();

        if (!result.next()) return null;
        return createUser(result);
    }

    @Override
    @Nullable
    @SneakyThrows
    public User findByEmail(@NotNull final String email) {
        @NotNull val query = "SELECT id, login, password, role, firstname, lastname, email, locked_flg FROM user WHERE email = ?;";
        @NotNull val prepareStatement = this.connection.prepareStatement(query);
        prepareStatement.setString(1, email);
        @NotNull val result = prepareStatement.executeQuery();

        if (!result.next()) return null;
        return createUser(result);
    }

    @Override
    @SneakyThrows
    public void editPassword(@NotNull final Long id, @NotNull final String hashedPassword) {
        @NotNull val query = "UPDATE user SET password = ? WHERE id = ?;";
        @NotNull val prepareStatement = this.connection.prepareStatement(query);

        prepareStatement.setString(1, hashedPassword);
        prepareStatement.setLong(2, id);

        prepareStatement.executeUpdate();
    }

    @Override
    @SneakyThrows
    public void editUserInfo(@NotNull final Long id, @NotNull final String firstName,
                             @Nullable final String lastName) {
        @NotNull val query = "UPDATE user SET firstname = ?, lastname = ? WHERE id = ?;";
        @NotNull val prepareStatement = this.connection.prepareStatement(query);

        prepareStatement.setString(1, firstName);
        prepareStatement.setString(2, lastName);
        prepareStatement.setLong(3, id);

        prepareStatement.executeUpdate();
    }

    @Override
    @SneakyThrows
    public void deleteByLogin(@NotNull final String login) {
        @NotNull val query = "DELETE FROM user WHERE login = ?;";
        @NotNull val prepareStatement = this.connection.prepareStatement(query);
        prepareStatement.setString(1, login);
        prepareStatement.executeUpdate();
    }

    @Override
    @SneakyThrows
    public void lockById(@NotNull final Long id) {
        lockUnlockById(id, true);
    }

    @Override
    @SneakyThrows
    public void unlockById(@NotNull final Long id) {
        lockUnlockById(id, false);
    }

    @Override
    @SneakyThrows
    public void lockByLogin(@NotNull final String login) {
        lockUnlockByLogin(login, true);
    }

    @Override
    @SneakyThrows
    public void unlockByLogin(@NotNull final String login) {
        lockUnlockByLogin(login, false);
    }

    @NotNull
    @SneakyThrows
    private User createUser(@NotNull final ResultSet result) {
        @NotNull val user = new User();
        user.setId(result.getLong(1));
        user.setLogin(result.getString(2));
        user.setHashedPassword(result.getString(3));
        user.setRole(Role.valueOf(result.getString(4)));
        user.setFirstName(result.getString(5));
        user.setLastName(result.getString(6));
        user.setEmail(result.getString(7));
        user.setLocked(result.getBoolean(8));
        return user;
    }

    @SneakyThrows
    private void lockUnlockById(@NotNull final Long id, final boolean lockedUnlockedFlag) {
        @NotNull val query = "UPDATE user SET locked_flg = ? WHERE id = ?;";
        @NotNull val prepareStatement = this.connection.prepareStatement(query);
        prepareStatement.setBoolean(1, lockedUnlockedFlag);
        prepareStatement.setLong(2, id);
        prepareStatement.executeUpdate();
    }

    @SneakyThrows
    private void lockUnlockByLogin(@NotNull final String login, final boolean lockedUnlockedFlag) {
        @NotNull val query = "UPDATE user SET locked_flg = ? WHERE login = ?;";
        @NotNull val prepareStatement = this.connection.prepareStatement(query);
        prepareStatement.setBoolean(1, lockedUnlockedFlag);
        prepareStatement.setString(2, login);
        prepareStatement.executeUpdate();
    }

}
