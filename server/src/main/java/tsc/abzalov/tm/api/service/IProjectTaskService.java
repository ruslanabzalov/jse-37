package tsc.abzalov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.model.Project;
import tsc.abzalov.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    int indexOf(@Nullable Long userId, @Nullable Task task);

    boolean hasData(@Nullable Long userId);

    void addTaskToProjectById(@Nullable Long userId, @Nullable Long projectId,
                              @Nullable Long taskId);

    @Nullable
    Project findProjectById(@Nullable Long id);

    @Nullable
    Task findTaskById(@Nullable Long id);

    @NotNull
    List<Task> findProjectTasksById(@Nullable Long userId, @Nullable Long projectId);

    void deleteProjectById(@Nullable Long id);

    void deleteProjectTasksById(@Nullable Long userId, @Nullable Long projectId);

    void deleteProjectTaskById(@Nullable Long userId, @Nullable Long projectId);

}
