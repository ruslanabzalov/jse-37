package tsc.abzalov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.model.Project;
import tsc.abzalov.tm.model.Session;
import tsc.abzalov.tm.model.Task;

import java.util.List;

public interface IProjectTaskEndpoint {

    int indexOfProjectTask(@Nullable Session session, @Nullable Task task);

    boolean hasData(@Nullable Session session);

    void addTaskToProjectById(@Nullable Session session, @Nullable Long projectId,
                              @Nullable Long taskId);

    @Nullable
    Project findTaskProjectById(@Nullable Session session, @Nullable Long id);

    @Nullable
    Task findProjectTaskById(@Nullable Session session, @Nullable Long id);

    @NotNull
    List<Task> findProjectTasksById(@Nullable Session session, @Nullable Long projectId);

    void deleteProjectById(@Nullable Session session, @Nullable Long id);

    void deleteProjectTasksById(@Nullable Session session, @Nullable Long projectId);

    void deleteProjectTaskById(@Nullable Session session, @Nullable Long projectId);

}
