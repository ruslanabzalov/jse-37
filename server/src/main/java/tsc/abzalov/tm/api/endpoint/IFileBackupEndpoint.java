package tsc.abzalov.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.model.Session;

public interface IFileBackupEndpoint {

    void autoLoadBackup(@Nullable Session session);

    void autoSaveBackup(@Nullable Session session);

    void base64LoadBackup(@Nullable Session session);

    void base64SaveBackup(@Nullable Session session);

    void binaryLoadBackup(@Nullable Session session);

    void binarySaveBackup(@Nullable Session session);

    void fasterXmlJsonLoadBackup(@Nullable Session session);

    void fasterXmlJsonSaveBackup(@Nullable Session session);

    void fasterXmlLoadBackup(@Nullable Session session);

    void fasterXmlSaveBackup(@Nullable Session session);

    void fasterXmlYamlLoadBackup(@Nullable Session session);

    void fasterXmlYamlSaveBackup(@Nullable Session session);

    void jaxbJsonLoadBackup(@Nullable Session session);

    void jaxbJsonSaveBackup(@Nullable Session session);

    void jaxbXmlLoadBackup(@Nullable Session session);

    void jaxbXmlSaveBackup(@Nullable Session session);

}
