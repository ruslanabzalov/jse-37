package tsc.abzalov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IRepository<T> {

    long size();

    boolean isEmpty();

    void create(@NotNull T entity);

    void addAll(@NotNull List<T> entities);

    @NotNull
    List<T> findAll();

    @Nullable
    T findById(@NotNull Long id);

    void clear();

    void removeById(@NotNull Long id);

}
