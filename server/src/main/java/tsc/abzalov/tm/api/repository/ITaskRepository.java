package tsc.abzalov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import tsc.abzalov.tm.api.IBusinessEntityRepository;
import tsc.abzalov.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IBusinessEntityRepository<Task> {

    void addTaskToProjectById(@NotNull Long userId, @NotNull Long projectId, @NotNull Long taskId);

    @NotNull
    List<Task> findProjectTasksById(@NotNull Long userId, @NotNull Long projectId);

    void deleteProjectTasksById(@NotNull Long userId, @NotNull Long projectId);

    void deleteProjectTaskById(@NotNull Long userId, @NotNull Long projectId);

}
