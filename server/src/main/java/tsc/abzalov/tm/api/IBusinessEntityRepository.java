package tsc.abzalov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.model.AbstractBusinessEntity;

import java.util.List;

public interface IBusinessEntityRepository<T extends AbstractBusinessEntity> extends IRepository<T> {

    long size(@NotNull Long userId);

    boolean isEmpty(@NotNull Long userId);

    int indexOf(@NotNull Long userId, @NotNull T entity);

    @NotNull
    List<T> findAll(@NotNull Long userId);

    @Nullable
    T findById(@NotNull Long id);

    @Nullable
    T findByIndex(@NotNull Long userId, int index);

    @Nullable
    T findByName(@NotNull Long userId, @NotNull String name);

    void editById(@NotNull Long id, @NotNull String name,
               @NotNull String description);

    void editByName(@NotNull Long userId, @NotNull String name, @NotNull String description);

    void clear(@NotNull Long userId);

    void removeById(@NotNull Long id);

    void removeByName(@NotNull Long userId, @NotNull String name);

    void startById(@NotNull Long id);

    void endById(@NotNull Long id);

    @NotNull
    List<T> sortByName(@NotNull Long userId);

    @NotNull
    List<T> sortByStartDate(@NotNull Long userId);

    @NotNull
    List<T> sortByEndDate(@NotNull Long userId);

    @NotNull
    List<T> sortByStatus(@NotNull Long userId);

}
