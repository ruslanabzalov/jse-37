package tsc.abzalov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.IService;
import tsc.abzalov.tm.enumeration.Role;
import tsc.abzalov.tm.model.User;

public interface IUserService extends IService<User> {

    void create(@Nullable String login, @Nullable String password, @Nullable Role role,
                @Nullable String firstName, @Nullable String lastName, @Nullable String email);

    void create(@Nullable String login, @Nullable String password,@Nullable String firstName,
                @Nullable String lastName, @Nullable String email);

    boolean isUserExist(@Nullable String login, @Nullable String email);

    @Nullable
    User findByLogin(@Nullable String login);

    @Nullable
    User editPasswordById(@Nullable Long id, @Nullable String newPassword);

    @Nullable
    User editUserInfoById(@Nullable Long id, @Nullable String firstName, @Nullable String lastName);

    void deleteByLogin(@Nullable String login);

    @Nullable
    User lockById(@Nullable Long id);

    @Nullable
    User unlockById(@Nullable Long id);

    @Nullable
    User lockByLogin(@Nullable String login);

    @Nullable
    User unlockByLogin(@Nullable String login);

}
