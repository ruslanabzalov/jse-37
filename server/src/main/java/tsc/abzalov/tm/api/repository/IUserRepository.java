package tsc.abzalov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.IRepository;
import tsc.abzalov.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    User findByLogin(@NotNull String login);

    @Nullable
    User findByEmail(@NotNull String email);

    @Nullable
    void editPassword(@NotNull Long id, @NotNull String hashedPassword);

    @Nullable
    void editUserInfo(@NotNull Long id, @NotNull String firstName, @Nullable String lastName);

    void deleteByLogin(@NotNull String login);

    @Nullable
    void lockById(@NotNull Long id);

    @Nullable
    void unlockById(@NotNull Long id);

    @Nullable
    void lockByLogin(@NotNull String login);

    @Nullable
    void unlockByLogin(@NotNull String login);

}
