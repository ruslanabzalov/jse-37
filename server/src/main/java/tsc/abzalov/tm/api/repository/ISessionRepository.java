package tsc.abzalov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.model.Session;

public interface ISessionRepository {

    void addSession(@NotNull Session session);

    void removeSession(@NotNull final Long id);

    @Nullable
    Session findSession(@NotNull final Long id);

}
