package tsc.abzalov.tm.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.enumeration.Role;

import java.io.Serializable;
import java.util.Optional;

import static tsc.abzalov.tm.enumeration.Role.USER;
import static tsc.abzalov.tm.util.LiteralConst.*;

@Data
@EqualsAndHashCode(callSuper = true)
public final class User extends AbstractEntity implements Serializable, Cloneable {

    @Nullable
    private String login;

    @Nullable
    private String hashedPassword;

    @NotNull
    private Role role = USER;

    @Nullable
    private String firstName;

    @Nullable
    private String lastName;

    @Nullable
    private String email;

    private boolean isLocked = false;

    @Nullable
    @Override
    public User clone() {
        try {
            return (User) super.clone();
        } catch (@NotNull final CloneNotSupportedException exception) {
            return null;
        }
    }

    @NotNull
    @Override
    public String toString() {
        @NotNull val correctLogin = Optional.ofNullable(login).orElse(DEFAULT_LOGIN);
        @NotNull val correctRoleName = role.getRoleName();
        @NotNull val correctFirstName = Optional.ofNullable(firstName).orElse(DEFAULT_FIRSTNAME);
        @NotNull val correctLastName = Optional.ofNullable(lastName).orElse(DEFAULT_LASTNAME);
        @NotNull val correctEmail = Optional.ofNullable(email).orElse(DEFAULT_EMAIL);
        @NotNull val correctUserStatus = (isLocked) ? LOCKED : ACTIVE;

        return "[ID: " + getId() +
                "; Login: " + correctLogin +
                "; Role: " + correctRoleName +
                "; First Name: " + correctFirstName +
                "; Last Name: " + correctLastName +
                "; Email: " + correctEmail +
                "; Status: " + correctUserStatus + "]";
    }

}
