package tsc.abzalov.tm.service;

import lombok.SneakyThrows;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IConnectionService;
import tsc.abzalov.tm.api.service.IUserService;
import tsc.abzalov.tm.api.service.property.IHashingPropertyService;
import tsc.abzalov.tm.enumeration.Role;
import tsc.abzalov.tm.exception.auth.*;
import tsc.abzalov.tm.exception.data.EmptyIdException;
import tsc.abzalov.tm.exception.data.EntityNotFoundException;
import tsc.abzalov.tm.model.User;
import tsc.abzalov.tm.repository.UserRepository;

import java.util.List;
import java.util.Optional;

import static tsc.abzalov.tm.enumeration.Role.USER;
import static tsc.abzalov.tm.util.HashUtil.hash;

public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final IConnectionService connectionService;

    @NotNull
    private final IHashingPropertyService propertyService;

    public UserService(@NotNull final IConnectionService connectionService,
                       @NotNull final IHashingPropertyService propertyService) {
        this.connectionService = connectionService;
        this.propertyService = propertyService;
    }

    @Override
    @SneakyThrows
    public long size() {
        @NotNull val connection = this.connectionService.getConnection();
        @NotNull val userRepository = new UserRepository(connection);
        return userRepository.size();
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    @SneakyThrows
    public void create(@Nullable final User user) {
        if (user == null) throw new EntityNotFoundException();

        checkMainInfo(user.getLogin(), user.getHashedPassword(), user.getFirstName(), user.getEmail());

        @NotNull val connection = this.connectionService.getConnection();
        @NotNull val userRepository = new UserRepository(connection);

        try {
            userRepository.create(user);
            connection.commit();
        } catch (Exception exception) {
            exception.printStackTrace();
            connection.rollback();
        } finally {
            connection.close();
        }
    }

    @Override
    public void create(@Nullable final String login, @Nullable final String password, @Nullable final Role role,
                       @Nullable final String firstName, @Nullable final String lastName,
                       @Nullable final String email) {
        create(createUser(login, password, role, firstName, lastName, email));
    }

    @Override
    public void create(@Nullable final String login, @Nullable final String password,
                       @Nullable final String firstName, @Nullable final String lastName,
                       @Nullable final String email) {
        create(createUser(login, password, USER, firstName, lastName, email));
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable final List<User> users) {
        if (users == null) throw new EntityNotFoundException();

        @NotNull val connection = this.connectionService.getConnection();
        @NotNull val userRepository = new UserRepository(connection);

        for (@NotNull val user : users) {
            try {
                userRepository.create(user);
                connection.commit();
            } catch (Exception exception) {
                exception.printStackTrace();
                connection.rollback();
            } finally {
                connection.close();
            }
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findById(@Nullable final Long id) {
        if (id == null) throw new EmptyIdException();

        @NotNull val connection = this.connectionService.getConnection();
        @NotNull val userRepository = new UserRepository(connection);

        return Optional.ofNullable(userRepository.findById(id)).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @SneakyThrows
    public boolean isUserExist(@Nullable String login, @Nullable String email) {
        login = Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        email = Optional.ofNullable(email).orElseThrow(EmptyEmailException::new);

        @NotNull val connection = this.connectionService.getConnection();
        @NotNull val userRepository = new UserRepository(connection);

        val isExistByLogin = userRepository.findByLogin(login) != null;
        val isExistByEmail = userRepository.findByEmail(email) != null;
        return isExistByLogin && isExistByEmail;
    }

    @Override
    @Nullable
    @SneakyThrows
    public User findByLogin(@Nullable String login) {
        login = Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);

        @NotNull val connection = this.connectionService.getConnection();
        @NotNull val userRepository = new UserRepository(connection);

        @Nullable val searchedUser = userRepository.findByLogin(login);
        return Optional.ofNullable(searchedUser).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @Nullable
    @SneakyThrows
    public User editPasswordById(@Nullable Long id, @Nullable String newPassword) {
        id = Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        newPassword = Optional.ofNullable(newPassword).orElseThrow(IncorrectCredentialsException::new);

        @Nullable val counter = propertyService.getPasswordCounterProperty();
        @NotNull val salt = propertyService.getPasswordSaltProperty();

        @NotNull val connection = this.connectionService.getConnection();
        @NotNull val userRepository = new UserRepository(connection);

        try {
            userRepository.editPassword(id, hash(newPassword, counter, salt));
            connection.commit();
        } catch (Exception exception) {
            exception.printStackTrace();
            connection.rollback();
        }

        return Optional.ofNullable(userRepository.findById(id)).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @Nullable
    @SneakyThrows
    public User editUserInfoById(@Nullable Long id, @Nullable String firstName, @Nullable final String lastName) {
        id = Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        firstName = Optional.ofNullable(firstName).orElseThrow(EmptyFirstNameException::new);

        @NotNull val connection = this.connectionService.getConnection();
        @NotNull val userRepository = new UserRepository(connection);

        try {
            userRepository.editUserInfo(id, firstName, lastName);
            connection.commit();
        } catch (Exception exception) {
            exception.printStackTrace();
            connection.rollback();
        }

        return Optional.ofNullable(userRepository.findById(id)).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @SneakyThrows
    public void deleteByLogin(@Nullable String login) {
        login = Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);

        @NotNull val connection = this.connectionService.getConnection();
        @NotNull val userRepository = new UserRepository(connection);

        try {
            userRepository.deleteByLogin(login);
            connection.commit();
        } catch (Exception exception) {
            exception.printStackTrace();
            connection.rollback();
        }
    }

    @Override
    @Nullable
    @SneakyThrows
    public User lockById(@Nullable Long id) {
        id = Optional.ofNullable(id).orElseThrow(EmptyIdException::new);

        @NotNull val connection = this.connectionService.getConnection();
        @NotNull val userRepository = new UserRepository(connection);

        try {
            userRepository.lockById(id);
            connection.commit();
        } catch (Exception exception) {
            exception.printStackTrace();
            connection.rollback();
        }

        return Optional.ofNullable(userRepository.findById(id)).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @Nullable
    @SneakyThrows
    public User unlockById(@Nullable Long id) {
        id = Optional.ofNullable(id).orElseThrow(EmptyIdException::new);

        @NotNull val connection = this.connectionService.getConnection();
        @NotNull val userRepository = new UserRepository(connection);

        try {
            userRepository.unlockById(id);
            connection.commit();
        } catch (Exception exception) {
            exception.printStackTrace();
            connection.rollback();
        }

        return Optional.ofNullable(userRepository.findById(id)).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @Nullable
    @SneakyThrows
    public User lockByLogin(@Nullable String login) {
        login = Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);

        @NotNull val connection = this.connectionService.getConnection();
        @NotNull val userRepository = new UserRepository(connection);

        try {
            userRepository.lockByLogin(login);
            connection.commit();
        } catch (Exception exception) {
            exception.printStackTrace();
            connection.rollback();
        }

        return Optional.ofNullable(userRepository.findByLogin(login)).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @Nullable
    @SneakyThrows
    public User unlockByLogin(@Nullable String login) {
        login = Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);

        @NotNull val connection = this.connectionService.getConnection();
        @NotNull val userRepository = new UserRepository(connection);

        try {
            userRepository.unlockByLogin(login);
            connection.commit();
        } catch (Exception exception) {
            exception.printStackTrace();
            connection.rollback();
        }

        return Optional.ofNullable(userRepository.findByLogin(login)).orElseThrow(EntityNotFoundException::new);
    }

    @SneakyThrows
    private void checkMainInfo(@Nullable String login, @Nullable String password,
                               @Nullable String firstName, @Nullable String email) {
        Optional.ofNullable(login).orElseThrow(IncorrectCredentialsException::new);
        Optional.ofNullable(password).orElseThrow(IncorrectCredentialsException::new);
        Optional.ofNullable(firstName).orElseThrow(EmptyFirstNameException::new);
        Optional.ofNullable(email).orElseThrow(EmptyEmailException::new);
    }

    @NotNull
    @SneakyThrows
    private User createUser(@Nullable String login, @Nullable String password,
                            @Nullable Role role, @Nullable String firstName,
                            @Nullable String lastName, @Nullable String email) {
        checkMainInfo(login, password, firstName, email);

        @NotNull val counter = propertyService.getPasswordCounterProperty();
        @NotNull val salt = propertyService.getPasswordSaltProperty();

        @NotNull val user = new User();
        user.setLogin(login);
        password = Optional.ofNullable(password).orElseThrow(EmptyPasswordException::new);
        user.setHashedPassword(hash(password, counter, salt));
        if (role != null) user.setRole(role);
        else user.setRole(USER);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setEmail(email);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<User> findAll() {
        @NotNull val connection = this.connectionService.getConnection();
        @NotNull val userRepository = new UserRepository(connection);
        return userRepository.findAll();
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull val connection = this.connectionService.getConnection();
        @NotNull val userRepository = new UserRepository(connection);

        try {
            userRepository.clear();
            connection.commit();
        } catch (@NotNull final Exception exception) {
            exception.printStackTrace();
            connection.rollback();
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@Nullable final Long id) {
        if (id == null) throw new EmptyIdException();

        @NotNull val connection = this.connectionService.getConnection();
        @NotNull val userRepository = new UserRepository(connection);

        try {
            userRepository.removeById(id);
            connection.commit();
        } catch (@NotNull final Exception exception) {
            exception.printStackTrace();
            connection.rollback();
        }
    }

}
