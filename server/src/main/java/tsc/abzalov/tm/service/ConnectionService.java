package tsc.abzalov.tm.service;

import lombok.SneakyThrows;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import tsc.abzalov.tm.api.service.IConnectionService;
import tsc.abzalov.tm.api.service.property.IDatabasePropertyService;

import java.sql.Connection;
import java.sql.DriverManager;

public final class ConnectionService implements IConnectionService {

    @NotNull
    private final IDatabasePropertyService propertyService;

    public ConnectionService(@NotNull final IDatabasePropertyService propertyService) {
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Connection getConnection() {
        @NotNull val url = propertyService.getDatabaseUrl();
        @NotNull val user = propertyService.getDatabaseUser();
        @NotNull val password = propertyService.getDatabasePassword();
        @NotNull val connection = DriverManager.getConnection(url, user, password);
        connection.setAutoCommit(false);
        return connection;
    }

}
