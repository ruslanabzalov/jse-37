package tsc.abzalov.tm.api.service.property;

import org.jetbrains.annotations.NotNull;

public interface IDeveloperInfoPropertyService {

    @NotNull
    String getDeveloperName();

    @NotNull
    String getDeveloperEmail();

}
