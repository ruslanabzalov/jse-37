package tsc.abzalov.tm.exception.auth;

import tsc.abzalov.tm.exception.AbstractException;

public final class UserLockedException extends AbstractException {

    public UserLockedException() {
        super("User is locked! Please, contact your administrator.");
    }
}
