package tsc.abzalov.tm.command.project;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;

import static tsc.abzalov.tm.enumeration.CommandType.PROJECT_COMMAND;
import static tsc.abzalov.tm.util.InputUtil.inputDescription;
import static tsc.abzalov.tm.util.InputUtil.inputName;


public final class ProjectCreateCommand extends AbstractCommand {

    public ProjectCreateCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    public String getCommandName() {
        return "create-project";
    }

    @Nullable
    @Override
    public String getCommandArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Create project.";
    }

    @NotNull
    @Override
    public CommandType getCommandType() {
        return PROJECT_COMMAND;
    }

    @Override
    public void execute() {
        System.out.println("PROJECT CREATION");
        @NotNull val name = inputName();
        @NotNull val description = inputDescription();

        @NotNull val projectEndpoint = getServiceLocator().getProjectEndpoint();
        @NotNull val session = getServiceLocator().getSession();

        projectEndpoint.createProject(session, name, description);

        System.out.println("Projects was successfully created.\n");
    }

}
