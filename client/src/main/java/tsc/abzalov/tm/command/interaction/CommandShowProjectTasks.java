package tsc.abzalov.tm.command.interaction;

import lombok.val;
import lombok.var;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;
import tsc.abzalov.tm.util.InputUtil;

import static tsc.abzalov.tm.enumeration.CommandType.INTERACTION_COMMAND;


public final class CommandShowProjectTasks extends AbstractCommand {

    public CommandShowProjectTasks(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    public String getCommandName() {
        return "show-project-tasks";
    }

    @Nullable
    @Override
    public String getCommandArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show project tasks.";
    }

    @NotNull
    @Override
    public CommandType getCommandType() {
        return INTERACTION_COMMAND;
    }

    @Override
    public void execute() {
        System.out.println("SHOW PROJECT TASKS");
        @NotNull val projectTaskEndpoint = getServiceLocator().getProjectTaskEndpoint();
        @NotNull val session = getServiceLocator().getSession();
        val isDataExist = projectTaskEndpoint.hasData(session);
        if (isDataExist) {
            System.out.println("Project");
            @NotNull val projectId = InputUtil.inputId();
            System.out.println();

            @NotNull val projectTasks =
                    projectTaskEndpoint.findProjectTasksById(session, projectId);
            val areTasksNotExist = projectTasks.size() == 0;
            if (areTasksNotExist) {
                System.out.println("Tasks list is empty.\n");
                return;
            }

            var taskIndex = 0;
            for (@NotNull val task : projectTasks) {
                taskIndex = projectTaskEndpoint.indexOfProjectTask(session, task) + 1;
                System.out.println(taskIndex + ". " + task);
            }

            System.out.println();
            return;
        }

        System.out.println("One of the lists is empty!\n");
    }

}
