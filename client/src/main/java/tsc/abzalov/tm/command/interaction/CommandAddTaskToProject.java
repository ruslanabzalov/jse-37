package tsc.abzalov.tm.command.interaction;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;
import tsc.abzalov.tm.util.InputUtil;

import java.util.Optional;

import static tsc.abzalov.tm.enumeration.CommandType.INTERACTION_COMMAND;


public final class CommandAddTaskToProject extends AbstractCommand {

    public CommandAddTaskToProject(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public String getCommandName() {
        return "add-task-to-project";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Add task to project.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return INTERACTION_COMMAND;
    }

    @Override
    public void execute() {
        System.out.println("ADD TASK TO PROJECT");
        System.out.println("Project");
        @NotNull val projectId = InputUtil.inputId();
        @NotNull val projectTaskEndpoint = getServiceLocator().getProjectTaskEndpoint();

        @NotNull val session = getServiceLocator().getSession();
        @Nullable val searchedProject =
                projectTaskEndpoint.findTaskProjectById(session, projectId);
        val isProjectNotExist = !Optional.ofNullable(searchedProject).isPresent();
        if (isProjectNotExist) {
            System.out.println("Project was not found.\n");
            return;
        }

        System.out.println("Task");
        @NotNull val taskId = InputUtil.inputId();

        @Nullable val searchedTask =
                projectTaskEndpoint.findProjectTaskById(session, taskId);
        val isTaskNotExist = !Optional.ofNullable(searchedTask).isPresent();
        if (isTaskNotExist) {
            System.out.println("Task was not found.\n");
            return;
        }

        projectTaskEndpoint.addTaskToProjectById(session, projectId, taskId);
        System.out.println("Task was successfully added to the project.\n");
    }

}
