package tsc.abzalov.tm.command.task;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;

import java.util.Optional;

import static tsc.abzalov.tm.enumeration.CommandType.TASK_COMMAND;
import static tsc.abzalov.tm.util.InputUtil.inputId;


public final class TaskShowByIdCommand extends AbstractCommand {

    public TaskShowByIdCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public String getCommandName() {
        return "show-task-by-id";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show task by id.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return TASK_COMMAND;
    }

    @Override
    public void execute() {
        System.out.println("FIND TASK BY ID");
        @NotNull val taskEndpoint = getServiceLocator().getTaskEndpoint();
        @NotNull val session = getServiceLocator().getSession();

        val areTasksExist = taskEndpoint.tasksSize(session) != 0;
        if (areTasksExist) {
            @NotNull val taskId = inputId();
            System.out.println();

            @Nullable val searchedTask = taskEndpoint.findTaskById(session, taskId);
            val isTaskExist = Optional.ofNullable(searchedTask).isPresent();
            if (isTaskExist) {
                val taskIndex = taskEndpoint.taskIndex(session, searchedTask) + 1;
                System.out.println(taskIndex + ". " + searchedTask + "\n");
                return;
            }

            System.out.println("Searched task was not found.\n");
            return;
        }

        System.out.println("Tasks list is empty.\n");
    }

}
